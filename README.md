To start generating stagers and staging dialogs, please do the following:

1) Download and install Dr.Racket from http://download.racket-lang.org/

2) Uncomment the dialog specification in notation based on programming language
concepts given in stager-generator-testcases.rkt for which you desire to
generate a stager (one specification in this notation is uncommented by
default).

3) Run the following command-line (assuming a UNIX environment and assuming
Racket is in the PATH):

$ racket stager-generator-testcases.rkt

or run stager-generator-testcases.rkt from the DrRacket IDE (i.e., load the
program and click the "Run" button in the upper right hand corner of the IDE
window).

*** (4) IS IMPORTANT FOR GETTING THE DEMO TO WORK ***

4) For purposes of simplicity, and because the stager generator is a proof of
concept, in the absence of dialog responses, rather than responding to a
question (e.g., size) with an actual valid response (e.g., small), simply
respond with the question label (e.g., "size").  Here the question label
represents any valid response to that question.

For instance, the following is a sample transcript of the ("PE*" size blend
cream) dialog:

$ racket stager-generator-testcases.rkt
'(#t)

You may input PE*: (size blend cream)
Utterance 1: blend
Utterance 1 entered as: (blend)

You may input PE*: (size cream)
Utterance 2: cream
Utterance 2 entered as: (cream)

You may input PE*: (size)
Utterance 3: size
Utterance 3 entered as: (size)
Done.

While engaged in a dialog, in addition to responding, you may use the 'undo#'
and 'redo#' operations. For instance,

$ racket stager-generator-testcases.rkt
'(#t)

You may input PE*: (size blend cream)
Utterance 1: blend
Utterance 1 entered as: (blend)

You may input PE*: (size cream)
Utterance 2: undo#
Utterance 2 entered as: (undo#)

You may input PE*: (size blend cream)
Utterance 1: cream
Utterance 1 entered as: (cream)

You may input PE*: (size blend)
Utterance 2: size
Utterance 2 entered as: (size)

You may input PE*: (blend)
Utterance 3: undo#
Utterance 3 entered as: (undo#)

You may input PE*: (size blend)
Utterance 2: redo#
Utterance 2 entered as: (redo#)

You may input PE*: (blend)
Utterance 3: blend
Utterance 3 entered as: (blend)
Done.

5) If you desire to re-run the same stager, go back to step 3. Otherwise go
back to step 2 to generate a stager for a new dialog.

6) See README2 for details on an alternative approach to step 3 above.
